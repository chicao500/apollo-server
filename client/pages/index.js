import { withApollo } from "../lib/apollo";
import UserList from "../components/userList";

const IndexPage = () => (
  <div>
    <UserList />
  </div>
);

export default withApollo({ ssr: true })(IndexPage);
