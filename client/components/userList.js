import React from "react";
// import { useListUsersQuery } from "../generated/graphql";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const UserList = () => {
  const { loading, error, data } = useQuery(gql`
    query {
      users {
        id
        name
      }
    }
  `);
  if (loading) {
    return <div>loading ...</div>;
  }
  return (
    <div>
      <ul>
        {data.users.map((user) => {
          return <li key={user.id}>{user.name}</li>;
        })}
      </ul>
    </div>
  );
};

export default UserList;
