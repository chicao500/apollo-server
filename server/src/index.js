import { ApolloServer } from "apollo-server-express";
import express from "express";
import { typeDefs } from "./typeDefs";
import { userResolver } from "./resolvers/userResolver";
import { problemResolver } from "./resolvers/problemResolver";
import cors from "cors";

const app = express();

app.use(cors());

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers: [userResolver, problemResolver],
});

apolloServer.applyMiddleware({ app, cors: false });

app.listen(4000, () => {
  console.log("Servidor rodando na porta 4000");
});
