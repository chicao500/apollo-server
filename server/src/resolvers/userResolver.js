import { userModel } from "../models/userModel";

export const userResolver = {
  Query: {
    users() {
      return userModel.UserModel.get();
    },
    user(_, args) {
      return userModel.UserModel.getById(args.id);
    },
  },
};
