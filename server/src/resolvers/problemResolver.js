import { problemModel } from "../models/problemModel";
import connection from "../config/database";

export const problemResolver = {
  Query: {
    problem(_, args) {
      return problemModel.ProblemModel.getById(args.id);
    },
    problems() {
      return problemModel.ProblemModel.get();
    },
    problemsByUserId(parent, args) {
      return problemModel.ProblemModel.getByUser(args.id);
    },
  },
  Problem: {
    createdBy(parent) {
      return new Promise((resolve, reject) => {
        connection.query(
          `select * from user where id = ${parent.userId}`,
          (error, result) => {
            if (error) {
              reject(error);
            } else {
              resolve(result[0]);
            }
          }
        );
      });
    },
  },
};
