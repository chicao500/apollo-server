const users = [
  { id: 1, name: "Pedro" },
  { id: 2, name: "Chico" },
  { id: 3, name: "Gustavo" },
];

export const userModel = {
  UserModel: {
    getById(userId) {
      return users.find((user) => user.id == userId);
    },
    get() {
      return users;
    },
  },
};
