import connection from "../config/database";

export const problemModel = {
  ProblemModel: {
    getById(problemId) {
      return new Promise((resolve, reject) => {
        connection.query(
          `select * from problem where id = ${problemId}`,
          (error, result) => {
            if (error) {
              reject(error);
            } else {
              resolve(result[0]);
            }
          }
        );
      });
    },
    get() {
      return new Promise((resolve, reject) => {
        connection.query(`select * from problem`, (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        });
      });
    },
    getByUser(userId) {
      return new Promise((resolve, reject) => {
        connection.query(
          `select * from problem where userId = ${userId}`,
          (error, result) => {
            if (error) {
              reject(error);
            } else {
              resolve(result);
            }
          }
        );
      });
    },
  },
};
