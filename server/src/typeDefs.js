const { gql } = require("apollo-server");

export const typeDefs = gql`
  type User {
    id: ID
    name: String
  }

  type Problem {
    id: ID
    problem: String
    createdBy: User
  }

  type Query {
    problem(id: ID!): Problem
    problems: [Problem]
    problemsByUserId(id: ID!): [Problem]
    users: [User]
    user(id: ID!): User
  }
`;
