import MySql from "mysql";

const connection = MySql.createPool({});

export default connection;
